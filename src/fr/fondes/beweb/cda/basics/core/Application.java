package fr.fondes.beweb.cda.basics.core;

import fr.fondes.beweb.cda.basics.core.helpers.FileBuffer;
import fr.fondes.beweb.cda.basics.core.helpers.SScanner;
import fr.fondes.beweb.cda.basics.core.helpers.SocketIO;
import fr.fondes.beweb.cda.basics.core.helpers.Source;

public class Application {

  // Faire une fabrique et un singleton
  public static void startServer(String app) throws Exception {

    Source source = null;
    if (app == "file") {
      source = new FileBuffer("./file.txt", true);
      source.handle();
    } else if (app == "socketIO") {
      source = new SocketIO();
      source.handle();
    } else if (app == "scanner") {
      source = new SScanner();
      source.handle();
    }
  }

  // public void startClient() throws Exception {

  // Socket s = new Socket("localhost", 114);

  // BufferedWriter bw = new BufferedWriter(new PrintWriter(s.getOutputStream()));

  // Scanner sc = new Scanner(System.in);
  // String message = "";

  // while (true) {
  // message = sc.nextLine();
  // if (message.equals("q"))
  // break;
  // System.out.println(message);
  // bw.write(message);
  // bw.newLine();
  // bw.flush();
  // }
  // sc.close();
  // }

}
