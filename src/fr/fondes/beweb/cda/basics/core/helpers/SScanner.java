package fr.fondes.beweb.cda.basics.core.helpers;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.ServerSocket;
import java.net.Socket;

public class SScanner extends Source {
    
    private ServerSocket serverSocket;
    private Socket client;
    
    
    @Override
    public void handle() throws IOException {
                // Setup
                this.setup();
        
                this.read();
        
                // Teardown
                this.teardown();
    }

    @Override
    public void read() throws IOException {
        BufferedReader br = new BufferedReader(new InputStreamReader(this.client.getInputStream()));

        String message = br.readLine();

        System.out.println(message);
    }

    @Override
    public void setup() throws IOException {
        System.out.println("Application start ");

        this.serverSocket = new ServerSocket(114);
        System.out.println("Attente du client...");
        
        this.client = this.serverSocket.accept();
        System.out.println(" Le client " + client.getInetAddress() + ":" + client.getPort() + " est connecté ");
    }

    @Override
    public void teardown() throws IOException {
        this.serverSocket.close();

    }


    @Override
    public void write() throws IOException {
        // TODO Auto-generated method stub
        
    }
}
