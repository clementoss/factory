package fr.fondes.beweb.cda.basics.core.helpers.interfaces;

import java.io.IOException;

public interface IConfig {

    public void handle() throws IOException;
    
    public void setup() throws IOException;

    public void teardown() throws IOException;

}
