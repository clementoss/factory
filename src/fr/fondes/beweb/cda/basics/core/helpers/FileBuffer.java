package fr.fondes.beweb.cda.basics.core.helpers;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Date;

public class FileBuffer extends Source {

    private FileReader inFile;
    private FileWriter outFile;
    private String fileName;
    private String content;
    private Boolean append;

    public FileBuffer(String fileName, Boolean append) {
        this.fileName = fileName;
        this.append = append;
    }

    @Override
    public void handle() throws IOException {
        // Setup
        this.setup();
        // Read
        this.read();
        // Write
        this.write();
        // Teardown
        this.teardown();
    }

    @Override
    public void setup() throws IOException {
        this.outFile = new FileWriter(this.fileName, this.append);
    }

    /**
     * Lis le fichier et renvoie une chaine de caractere
     */
    @Override
    public void read() throws IOException {

        try {
            this.inFile = new FileReader(this.fileName);
        } catch (FileNotFoundException e) {
            // Si pas de fichier il le crée
            File file = new File(this.fileName);
            file.createNewFile();
            this.inFile = new FileReader(file.getPath());
        }
        this.content = this.readContent();

    }

    public String readContent() {
        String content = "";
        int c = 0;
        try {
            while (c != -1) {
                c = this.inFile.read();
                content += (char) c;
            }
            this.inFile.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return content;
    }

    @Override
    public void write() throws IOException {
        this.outFile.write(new Date() + ": " + "\n");
    }

    @Override
    public void teardown() throws IOException {
        this.outFile.close();

    }

}
