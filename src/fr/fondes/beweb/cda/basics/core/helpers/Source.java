package fr.fondes.beweb.cda.basics.core.helpers;

import java.io.IOException;

import fr.fondes.beweb.cda.basics.core.helpers.interfaces.IConfig;
import fr.fondes.beweb.cda.basics.core.helpers.interfaces.IReadable;
import fr.fondes.beweb.cda.basics.core.helpers.interfaces.IWritable;

public abstract class Source implements IConfig, IReadable, IWritable {


}
