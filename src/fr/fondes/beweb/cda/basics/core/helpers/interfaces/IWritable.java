package fr.fondes.beweb.cda.basics.core.helpers.interfaces;

import java.io.IOException;

public interface IWritable {
    public void write() throws IOException;
}
