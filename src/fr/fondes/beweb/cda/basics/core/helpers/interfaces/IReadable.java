package fr.fondes.beweb.cda.basics.core.helpers.interfaces;

import java.io.IOException;

public interface IReadable {
    public void read() throws  IOException;
    
}
