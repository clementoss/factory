import fr.fondes.beweb.cda.basics.core.Application;

public class App {
    public static void main(String[] args) throws Exception {

        // Application.startServer("scanner");
        // Application.startServer("file");
        Application.startServer("socketIO");
    }
}
