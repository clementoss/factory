import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {
    public static void main(String[] args) throws Exception {

        Socket socket = new Socket("localhost", 114);

        System.out.println("Connected to server " + socket.getInetAddress()
                + ": " + socket.getPort());

        /**
         * SOCKET
         */
        socket(socket);

        /**
         * SCANNER
         */
        // scanner(socket);

        socket.close();

    }

    private static void socket(Socket socket) throws IOException {

        String message = new String("Hello from fucking Socket !");

        OutputStreamWriter osw = new OutputStreamWriter(socket.getOutputStream());
        osw.append(message);
        osw.flush();
    }

    public static void scanner(Socket socket) throws IOException {

        BufferedWriter bw = new BufferedWriter(new PrintWriter(socket.getOutputStream()));

        Scanner sc = new Scanner(System.in);
        String message = "";
        while (true) {
            message = sc.nextLine();
            if (message.equals("q"))
                break;
            bw.write(message);
            bw.newLine();
            bw.flush();
        }
        sc.close();

    }

}